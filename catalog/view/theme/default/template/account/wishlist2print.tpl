<!DOCTYPE html>
<html>
<head>
  <title><?php echo $heading_title; ?></title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <style type="text/css">
   *{font-family: 'Open Sans', sans-serif; transition:all 0.288s linear; -webkit-transition:all 0.288s linear; -moz-transition:all 0.288s linear; -o-transition:all 0.288s linear; -ms-transition:all 0.288s linear;}
   body{
    margin: 0;
    padding-top: 0;
   }
   /*h1{text-align: center; color:#8fd4b8 }
   h2{text-align: center; color:#f299a5 }
   table{margin: auto; font-size: 20px}
   table thead td{border:1px solid #8fd4b8; background: #8fd4b8; color:white;}
   table tbody{color:  #f299a5;}
   table tbody td{border:1px solid #8fd4b8;}
   td{text-align: center; font-size: 20px}
   a{color:  #f299a5; text-decoration: none; font-size: 20px}
   .buttons{width:100%; background: #8fd4b8}
   .btn-wrapper{width:168px; margin: auto; padding:5px;}
   .buttons .btn{float: left; width: 40px; background: #f299a5; text-align: center; padding: 5px; border-radius:4px; border:solid 1px #f299a5;}
   .buttons .l .btn{margin-left:10px;}
   .buttons .btn:hover{background: white;}
   .buttons .btn i{color: white;  font-size: 20px}
   .buttons .btn:hover i{color: #f299a5;}
   .bottom{background: #8fd4b8; color:white}*/
   
   h1{text-align: center; color:dimgray }
   h2{text-align: center; color:#38B0E3 }
   table{margin: auto; font-size: 20px}
   table thead td{border:1px solid #DBDEE1; background: #DBDEE1; color:dimgray;}
   table tbody{color:  #38B0E3;}
   table tbody td{border:1px solid #DBDEE1;}
   td{text-align: center; font-size: 20px}
   a{color:  #38B0E3; text-decoration: none; font-size: 20px}
   .buttons{width:100%; background: #DBDEE1}
   .btn-wrapper{width:176px; margin: auto; padding:5px;}
   .buttons .btn{float: left; width: 40px; background: #38B0E3; text-align: center; padding: 5px; border-radius:4px; border:solid 1px #38B0E3;}
   .buttons .l .btn{margin-left:10px;}
   .buttons .btn:hover{background: white;}
   .buttons .btn i{color: white;  font-size: 20px}
   .buttons .btn:hover i{color: #38B0E3;}
   .bottom{background: #DBDEE1; color:dimgray}
   

   body.neutral h1{color:black}
   body.neutral h2{color:black}
   body.neutral table thead td{border:1px solid black; background: black; color:white;}
   body.neutral table tbody{color:  black;}
   body.neutral table tbody td{border:1px solid black;}
   body.neutral table tbody td img{-webkit-filter: grayscale(100%); -moz-filter: grayscale(100%); -ms-filter: grayscale(100%); -o-filter: grayscale(100%); filter: grayscale(100%);}
   body.neutral a{color:  black;}
   body.neutral .buttons{width:100%; background: black}
   body.neutral .buttons .btn{background: white; border:solid 1px white;}
   body.neutral .buttons .btn:hover{background: black;}
   body.neutral .buttons .btn i{color: black;}
   body.neutral .buttons .btn:hover i{color: white;}
   body.neutral .bottom{background: black;}

   @media print {
    .buttons{display: none;}
   }
  </style>
</head>
<body>
<div id="content">
  <div class="buttons">
    <div class="btn-wrapper">
        <div><a href="javascript:void(0)" onclick="colorScheme('neutral')" id="scheme" class="btn"><i class="fa fa-paint-brush"></i></a></div>
        <div class="l"><a href="javascript:void(0)" onclick="window.print()" class="btn"><i class="fa fa-print"></i></a></div>
        <div class="l"><a href="javascript:void(0)" onclick="feedGen()" class="btn"><i class="fa fa-anchor"></i></a></div>
        <div style="clear:both"></div>
    </div>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <h2>www.site.be</h2>
  <?php if ($products) { ?>
  <div>
    <table width="900"  cellpadding="3">
      <thead>
        <tr>
          <td class="image"><?php echo $column_image; ?></td>
          <td class="name"><?php echo $column_name; ?></td>
          <td class="model"><?php echo $column_model; ?></td>
          <td class="stock"><?php echo $column_stock; ?></td>
          <td class="price"><?php echo $column_price; ?></td>
        </tr>
      </thead>
      <?php foreach ($products as $product) { ?>
      <tbody id="wishlist-row<?php echo $product['product_id']; ?>">
        <tr>
          <td><?php if ($product['thumb']) { ?>
            <a href="javascript:void(0)"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
            <?php } ?></td>
          <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>
          <td><?php echo $product['model']; ?></td>
          <td><?php echo $product['stock']; ?></td>
          <td><?php if ($product['price']) { ?>
            <div class="price">
              <?php if (!$product['special']) { ?>
              <?php echo $product['price']; ?>
              <?php } else { ?>
              <s><?php echo $product['price']; ?></s> <b><?php echo $product['special']; ?></b>
              <?php } ?>
            </div>
            <?php } ?></td>
        </tr>
      </tbody>
      <?php } ?>
        <tbody>
        <tr>
          <td colspan="5" class="bottom"><?php echo date("d/m/y | h:i"); ?></td>
        </tr>
        </tbody>
    </table>
  </div>
  
  <?php } else { ?>
 
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
</div>
</body>
<script type="text/javascript">
   function colorScheme(type)
   {
     $('body').attr('class', type);
     if (type == 'neutral') {
      $('#scheme').attr('onclick', 'colorScheme("colorful")');
     }
     else
     {
      $('#scheme').attr('onclick', 'colorScheme("neutral")');
     }
   }
   function feedGen()
   {
    //não esquecer de criar language
    var title = prompt("Enter the List Title:");
    if((title != ""))
    {
      $.post('index.php?route=account/wishlist/feed_generate', {title:title}, function(returned)
        {
          if(returned == "")
          {
             alert('There was an error! please try again later!');
          }
          else
          {
            //alert(returned);
            window.location.replace("http://localhost/defaultopencart/index.php?route=product/wishlist&feed_id="+returned);
          }
        });
    }
    else
    {
      alert("Type a title!");
    }
   }
</script>
</html>

