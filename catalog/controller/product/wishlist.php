<?php 
class ControllerProductWishList extends Controller {
	public function index() {	
		
        $feedId = $this->request->get['feed_id'];

		$this->language->load('account/wishlist');
		
		$this->load->model('catalog/product');
		
		$this->load->model('account/feed');
		
		$this->load->model('tool/image');
	    
	    $feed = $this->model_account_feed->getFeed($feedId);

	    $products = explode('|', $feed['products']);
		
		$this->document->setTitle($this->language->get('heading_title'));	
      	
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $feed['title'],
			'href'      => $this->url->link('product/wishlist', 'feed_id='.$feedId),
        	'separator' => $this->language->get('text_separator')
      	);
								
		$this->data['heading_title'] = $this->language->get('heading_title');	
		
		$this->data['text_empty'] = $this->language->get('text_empty');
     	
		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_model'] = $this->language->get('column_model');
		$this->data['column_stock'] = $this->language->get('column_stock');
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_action'] = $this->language->get('column_action');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['author'] = $feed['author'];
		$this->data['title'] = $feed['title'];
		$this->data['date'] = $feed['date_created'];
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
							
		$this->data['products'] = array();
		
		foreach ($products as $prod_id) {
			$prod = $this->model_catalog_product->getProduct($prod_id);
			if($prod)
			{
				if ($prod['image']) {
					$image = $this->model_tool_image->resize($prod['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'));
				} else {
					$image = false;
				}
				$this->data['products'][] = array
				(
					'product_id' => $prod['product_id'],
					'name' => $prod['name'],
					'model' => $prod['model'],
					'quantity' => $prod['quantity'],
					'stock_status' => $prod['stock_status'],
					'image' => $image,
					'price' => $prod['price'],
					'special' => $prod['special'],
					'href' => $this->url->link('product/product', 'product_id=' . $prod_id) 
				); 
				
			}
		}

		$this->data['continue'] = $this->url->link('common/home', '', 'SSL');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/wishlist.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/product/wishlist.tpl';
		} else {
			$this->template = 'default/template/product/wishlist.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
							
		$this->response->setOutput($this->render());		
	}
	

}
?>