<?php
class ModelAccountFeed extends Model {
	public function createFeed($products,$author,$title) {
		$prod_string = implode('|', $products);
		
		$query = $this->db->query("INSERT INTO `" . DB_PREFIX . "feed` (author, customer_id, products, store_id, date_created, date_modified, title) VAlUES ('" . $author . "', '" . $this->customer->getId() . "','" . $prod_string . "', '" . $this->config->get('config_store_id') . "',NOW(), NOW(), '" . $title . "')");
	    if ($query) {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
	}
	public function getFeed($id)
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "feed` WHERE feed_id = '" . $id . "'");
	    return $query->row;
	}
	public function getFeeds()
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "feed`");
	    return $query->rows;
	}	
	public function getNewFeed()
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "feed` WHERE customer_id = '" . $this->customer->getId() . "' ORDER BY date_modified DESC");
	    return $query->row;
	}	
}
?>