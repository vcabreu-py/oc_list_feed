<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/defaultopencart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/defaultopencart/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\defaultopencart/catalog/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\defaultopencart/system/');
define('DIR_DATABASE', 'C:\xampp\htdocs\defaultopencart/system/database/');
define('DIR_LANGUAGE', 'C:\xampp\htdocs\defaultopencart/catalog/language/');
define('DIR_TEMPLATE', 'C:\xampp\htdocs\defaultopencart/catalog/view/theme/');
define('DIR_CONFIG', 'C:\xampp\htdocs\defaultopencart/system/config/');
define('DIR_IMAGE', 'C:\xampp\htdocs\defaultopencart/image/');
define('DIR_CACHE', 'C:\xampp\htdocs\defaultopencart/system/cache/');
define('DIR_DOWNLOAD', 'C:\xampp\htdocs\defaultopencart/download/');
define('DIR_LOGS', 'C:\xampp\htdocs\defaultopencart/system/logs/');

// DB
define('DB_DRIVER', 'mysql');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencart');
define('DB_PREFIX', '');
?>